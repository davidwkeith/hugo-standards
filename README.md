# Standards Hugo Theme


### Front Matter

```yaml
params:
  legacyWellKnownVersion: 1 #FIXME: This should be the git modification date of each resource
  logo: /favicon.svg # Header logo
  logoAlt: "Pullets Forever Logo" # Header logo alt
  isAdultContent: false # toggles the [RTA Label](https://www.rtalabel.org) meta tag
```
